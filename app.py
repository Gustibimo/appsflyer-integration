import requests
import os
import json
import urllib

def main():
    api_endpoint = "https://hq.appsflyer.com/export/"
    api_token = "3dbe32f2-3756-49f0-a6c0-3e272303f7c5" 
    app_id = "com.bitsmedia.android.muslimpro"
    report_name = "partners_by_date_report"

    from_dt = "2020-09-08"
    to_dt = "2020-09-09"

    query_params = {
        "api_token": api_token,
        "from": str(from_dt),
        "to": str(to_dt)
        }

    query_string = urllib.parse.urlencode(query_params)

    request_url = api_endpoint + app_id + "/" + report_name + "/v5?" + query_string

    print(request_url)

    resp = urllib.request.urlopen(request_url)

    with open("appsflyer_installs_data.csv","wb") as fl:
        fl.write(resp.read())

if __name__ == "__main__":
    main()